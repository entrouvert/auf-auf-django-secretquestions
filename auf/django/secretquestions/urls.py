# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('',
                       url(r'questions/setup$',
                           'auf.django.secretquestions.views.setup_form',
                           name="sq_setup"), )
