# -*- coding: utf-8 -*-

from datetime import timedelta, datetime
import re

from django.shortcuts import redirect
from django.utils.translation import ugettext as _

from django.contrib import messages
try:
    from django.contrib.auth import get_user_model
except ImportError:
    from django.contrib.auth.models import User
    def get_user_model():
        return User

from .views import SecretQuestionWizard
from .conf import SQ_SESSION_KEY, SQ_TOKEN_TTL


def secret_questions_required(ttl=SQ_TOKEN_TTL):

    def _inner(view):

        def _wrapped(request, *args, **kwargs):
            session_token, url, date, user_pk = request.session.get(SQ_SESSION_KEY,
                                                           (None,
                                                            None,
                                                            datetime.now(),
                                                            None))
            get_token = request.GET.get(SQ_SESSION_KEY, None)
            date_max = date + timedelta(seconds=ttl)

            if session_token is None or get_token is None:
                wiz = SecretQuestionWizard(request)
                return wiz(request, *args, **kwargs)

            if date_max < datetime.now() or \
               not request.get_full_path().startswith(url):
                if request.method == "POST":
                    messages.error(request,
                                   _("Your modifications were canceled."))
                url = request.get_full_path()
                regex_no_session_key = "(.*)%s=[a..z0..9]*(.*)" % \
                    SQ_SESSION_KEY
                clean_url = re.sub(regex_no_session_key, "\\1", url)
                return redirect(clean_url)

            if session_token == get_token:
                request.secret_questions_user = get_user_model().objects.get(pk=user_pk)
                return view(request, *args, **kwargs)

            # should not be raised
            raise Exception('SQ')   # pragma: no cover
        return _wrapped
    return _inner
