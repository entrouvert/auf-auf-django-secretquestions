# -*- coding: utf-8 -*-

from django.http import HttpResponse

from auf.django.secretquestions.decorators import secret_questions_required

TTL = 1


def public(request):
    return HttpResponse("OK")


@secret_questions_required()
def private(request):
    return HttpResponse("OK")


@secret_questions_required(ttl=TTL)
def private_ttl(request):
    return HttpResponse("OK")
