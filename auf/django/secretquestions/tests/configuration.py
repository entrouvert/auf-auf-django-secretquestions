# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.conf import settings

from auf.django.secretquestions.models import Answer

from .common import SecretQuestionTest


class ConfigurationTest(SecretQuestionTest):
    """
    TestCase for setting questions/answers
    """

    def test_access_setup_questions_for_anonymous(self):
        """
        Check if you try to access setup page as anonymous, you're redirected
        to login page.
        """
        url = reverse('sq_setup')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual('Location' in response, True)
        self.assertEqual(settings.LOGIN_URL in response['Location'], True)

    def test_access_setup_questions_for_authenticated(self):
        """
        Check if setup page is accessible from authenticated people
        """
        self.assertEqual(self.client.login(username=self.username,
                                           password=self.password), True)

        url = reverse('sq_setup')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_setting_answer_for_one_question(self):
        """
        Check if the answer is really stored and crypted
        """
        raw_password = 'xxx'
        self.assertEqual(self.client.login(username=self.username,
                                           password=self.password), True)
        url = reverse('sq_setup')

        data = {'form-TOTAL_FORMS': u'1',
                'form-INITIAL_FORMS': u'0',
                'form-MAX_NUM_FORMS': u'',
                'form-0-question': self.question1.id,
                'form-0-secret': raw_password, }

        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
        answer = Answer.objects.get(user=self.user, question=self.question1)
        self.assertNotEqual(answer.secret, raw_password)
        self.assertNotEqual(answer.secret, '')

    def test_setting_2_same_questions(self):
        """
        Check if error is raised if you choose 2 same questions
        """
        raw_password = 'xxx'
        self.assertEqual(self.client.login(username=self.username,
                                           password=self.password), True)
        url = reverse('sq_setup')

        data = {'form-TOTAL_FORMS': u'2',
                'form-INITIAL_FORMS': u'0',
                'form-MAX_NUM_FORMS': u'',
                'form-0-question': self.question1.id,
                'form-0-secret': raw_password,
                'form-1-question': self.question1.id,
                'form-1-secret': raw_password, }

        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        answers = Answer.objects.filter(user=self.user)
        self.assertEqual(len(answers), 0)
        self.assertTrue(
            "Each question has to be different." in response.content)

    def test_setting_empty_answer_for_one_question(self):
        """
        Check if the answer is not empty
        """
        raw_password = ''
        self.assertEqual(self.client.login(username=self.username,
                                           password=self.password), True)
        url = reverse('sq_setup')

        data = {'form-TOTAL_FORMS': u'1',
                'form-INITIAL_FORMS': u'0',
                'form-MAX_NUM_FORMS': u'',
                'form-0-question': self.question1.id,
                'form-0-secret': raw_password, }

        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        with self.assertRaises(Answer.DoesNotExist):
            Answer.objects.get(user=self.user, question=self.question1)

    def test_check_reset(self):
        """
        Check if you have ever set answer, the form does not prepopulate
        this one.
        """
        raw_password = 'xxx'
        self.test_setting_answer_for_one_question()
        url = reverse('sq_setup')
        response = self.client.get(url)
        self.assertFalse(raw_password in response.content)
        answer = Answer.objects.get(user=self.user, question=self.question1)
        self.assertFalse(answer.secret in response.content)
