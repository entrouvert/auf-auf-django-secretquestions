# -*- coding: utf-8 -*-

import re

from django.test import TestCase
from django.test.client import Client

from django.contrib.auth.models import User

from auf.django.secretquestions.models import Question, Answer, crypt_answer


class SecretQuestionTest(TestCase):

    client = Client()
    username = 'paul'
    password = 'lemay'

    def setUp(self):
        self.create_user()
        self.create_questions()

    def create_user(self):
        self.user = User.objects.create(username=self.username)
        self.user.set_password(self.password)
        self.user.save()

    def create_questions(self):
        self.question1 = Question.objects.create(text="question1")
        self.question1.save()
        self.question2 = Question.objects.create(text="question2")
        self.question2.save()

    def create_answers(self):
        self.answer1 = Answer.objects.create(question=self.question1,
                                             secret=crypt_answer('one'),
                                             user=self.user)
        self.answer1.save()
        self.answer2 = Answer.objects.create(question=self.question2,
                                             secret=crypt_answer('two'),
                                             user=self.user)
        self.answer2.save()

    def _get_hashs(self, response):
        """
        Parse response to prepare POST according previous hash
        """
        regex_hash = 'name="(hash_[0-9])+" value="([a-z0-9]+)"'
        found = re.findall(regex_hash, response.content)
        hashs = {}
        for k, v in found:
            hashs.update({k: v})
        return hashs

    def get_response_from_final_step(self, url):
        self.create_answers()

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        data = {'wizard_step': 0,
                '0-username': self.username, }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.question1.text in response.content)

        # wrong response
        data = {'wizard_step': 1,
                '0-username': self.username,
                '1-raw_answer': 'wrong answer', }
        data.update(self._get_hashs(response))
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, 200)
        self.assertFalse('2-raw_answer' in response.content)

        # good response
        data = {'wizard_step': 1,
                '0-username': self.username,
                '1-raw_answer': 'one', }
        data.update(self._get_hashs(response))
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('2-raw_answer' in response.content)

        # good response
        data = {'wizard_step': 2,
                '0-username': self.username,
                '1-raw_answer': 'one',
                '2-raw_answer': 'two', }
        data.update(self._get_hashs(response))
        response = self.client.post(url, data)
        return response
