# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse

from auf.django.secretquestions import conf

from .common import SecretQuestionTest


class UseTest(SecretQuestionTest):
    """
    TestCase for accessing guarded pages.
    """

    def test_access_public_for_anonymous(self):
        """
        Check if you try to access public page as anonymous
        """
        url = reverse('sq_test_public')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('OK' in response.content)

    def test_access_public_for_authenticated(self):
        """
        Check if you try to access public page as authenticated
        """
        self.assertEqual(self.client.login(username=self.username,
                                           password=self.password), True)
        url = reverse('sq_test_public')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('OK' in response.content)

    def test_access_private_for_anonymous(self):
        """
        Check if you try to access private page as anonymous,
        you are asking for username
        """
        url = reverse('sq_test_private')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertFalse('OK' in response.content)
        self.assertTrue('username' in response.content)

    def test_access_private_for_authenticated(self):
        """
        Check if you try to access private page as authenticated,
        you are asking for first question
        """
        self.create_answers()
        self.assertEqual(self.client.login(username=self.username,
                                           password=self.password), True)
        url = reverse('sq_test_private')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertFalse('OK' in response.content)
        self.assertFalse('username' in response.content)

    def test_access_private_without_question(self):
        """
        Check if you try to access private page as authenticated,
        you don't have set any secret questions yet
        you are redirected to the page
        """
        self.assertEqual(self.client.login(username=self.username,
                                           password=self.password), True)
        url = reverse('sq_test_private')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        self.assertFalse('OK' in response.content)
        self.assertFalse('username' in response.content)
        self.assertTrue(url in response['location'])
        self.assertTrue(conf.SQ_SESSION_KEY in response['location'])

    def test_username_form(self):
        """
        Test form submission
        """
        url = reverse('sq_test_private')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        data = {'0-username': '', }
        response = self.client.post(url, data)
        self.assertTrue('This field is required' in response.content)
        data = {'0-username': 'fake', }
        response = self.client.post(url, data)
        self.assertTrue('Username not found' in response.content)
        data = {'0-username': self.username, }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(url in response['location'])
        self.assertTrue(conf.SQ_SESSION_KEY in response['location'])

    def test_question_form(self):
        """
        Test form submission
        """
        self.create_answers()
        url = reverse('sq_test_private')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        data = {'wizard_step': 0,
                '0-username': self.username, }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.question1.text in response.content)

        # wrong response
        data = {'wizard_step': 1,
                '0-username': self.username,
                '1-raw_answer': 'wrong answer', }
        data.update(self._get_hashs(response))
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, 200)
        self.assertFalse('2-raw_answer' in response.content)

        # good response
        data = {'wizard_step': 1,
                '0-username': self.username,
                '1-raw_answer': 'one', }
        data.update(self._get_hashs(response))
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('2-raw_answer' in response.content)

        # good response
        data = {'wizard_step': 2,
                '0-username': self.username,
                '1-raw_answer': 'one',
                '2-raw_answer': 'two', }
        data.update(self._get_hashs(response))
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
        self.assertTrue(url in response['location'])
        self.assertTrue(conf.SQ_SESSION_KEY in response['location'])

        response = self.client.get(response['location'])
        self.assertTrue('OK' in response.content)

    def test_inconsistent_data(self):
        """
        Test inconsistent data between steps
        wizard toolkit redirect on step with corrupted data
        """
        self.create_answers()
        url = reverse('sq_test_private')
        response = self.client.get(url)
        data = {'wizard_step': 0,
                '0-username': self.username, }
        response = self.client.post(url, data)
        self.assertTrue('1-raw_answer' in response.content)
        data = {'wizard_step': 1,
                '0-username': self.username,
                '1-raw_answer': 'one', }
        data.update(self._get_hashs(response))
        response = self.client.post(url, data)
        self.assertTrue('2-raw_answer' in response.content)
        data = {'wizard_step': 2,
                '0-username': self.username,
                '1-raw_answer': 'xxx',
                '2-raw_answer': 'two', }
        data.update(self._get_hashs(response))
        response = self.client.post(url, data)
        self.assertTrue('1-raw_answer' in response.content)
