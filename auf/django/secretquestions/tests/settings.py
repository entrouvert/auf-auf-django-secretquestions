# -*- coding: utf-8 -*-

USE_I18N = False

SECRET_KEY = 'secret'

ROOT_URLCONF = 'auf.django.secretquestions.tests.urls'

DATABASES = {'default':
            {'ENGINE': 'django.db.backends.sqlite3', 'NAME': ':memory:', }}

INSTALLED_APPS = ('django.contrib.auth',
                  'django.contrib.contenttypes',
                  'django.contrib.sessions',
                  'django.contrib.admin',
                  'south',
                  'auf.django.secretquestions',)
