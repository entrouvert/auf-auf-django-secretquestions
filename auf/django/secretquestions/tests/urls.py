from django.conf.urls.defaults import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns(
    '',
    (r'^secret/',
        include('auf.django.secretquestions.urls')),
    url(r'^test_public/',
        'auf.django.secretquestions.tests.views.public',
        name='sq_test_public'),
    url(r'^test_private/',
        'auf.django.secretquestions.tests.views.private',
        name='sq_test_private'),
    url(r'^test_private_ttl/',
        'auf.django.secretquestions.tests.views.private_ttl',
        name='sq_test_private_ttl'),)
