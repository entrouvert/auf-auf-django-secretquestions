# -*- coding: utf-8 -*-


import time

from django.core.urlresolvers import reverse

from .common import SecretQuestionTest
from .views import TTL


class TokenTest(SecretQuestionTest):
    """
    TestCase for token testing
    """

    def test_expiracy(self):
        """
        Check if you try to access page after X times.
        """
        url = reverse('sq_test_private_ttl')
        response = self.get_response_from_final_step(url)
        location = response['location']

        response = self.client.get(location)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('OK' in response.content)
        time.sleep(TTL+1)
        response = self.client.get(location)
        self.assertEqual(response.status_code, 302)
        redirect = response['location'].replace('http://testserver', '')
        self.assertEqual(redirect, url)

    def test_warning(self):
        """
        Check if you try to access page after X times.
        """
        url = reverse('sq_test_private_ttl')
        response = self.get_response_from_final_step(url)
        location = response['location']

        response = self.client.get(location)
        self.assertEqual(response.status_code, 200)
        self.assertTrue('OK' in response.content)
        time.sleep(TTL+1)
        response = self.client.post(location, {})
        self.assertEqual(response.status_code, 302)
        redirect = response['location'].replace('http://testserver', '')
        self.assertEqual(redirect, url)
        response = self.client.get(url)
        messages = [m.message for m in response.context['messages']]
        self.assertTrue("Your modifications were canceled." in messages)
