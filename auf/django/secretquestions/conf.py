# -*- coding: utf-8 -*-

from django.conf import settings

SQ_SESSION_KEY = getattr(settings, 'SQ_SESSION_KEY', 'sq_token')
SQ_TOKEN_TTL = getattr(settings, 'SQ_TOKEN_TTL', 60*3)
