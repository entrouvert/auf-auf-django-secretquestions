# -*- coding: utf-8 -*-

from django.db import models

try:
    from django.contrib.auth.models import get_hexdigest, check_password
    def crypt_answer(raw):
        import random
        algo = 'sha1'
        salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
        hsh = get_hexdigest(algo, salt, raw)
        return '%s$%s$%s' % (algo, salt, hsh)
except ImportError:
    from django.contrib.auth.hashers import make_password, check_password
    def crypt_answer(raw):
        return make_password(raw, hasher='sha1')


def check_answer(raw, crypted):
    return check_password(raw, crypted)


class Question(models.Model):
    text = models.CharField(max_length=255)

    def __unicode__(self):
        return self.text


class Answer(models.Model):
    user = models.ForeignKey('auth.User', related_name="secret_answers")
    question = models.ForeignKey('secretquestions.Question')
    secret = models.CharField(max_length=255)

    class Meta:
        unique_together = ('question', 'user')
