# -*- coding: utf-8 -*-

from datetime import datetime
from urlparse import urlparse, parse_qs
from urllib import urlencode

from django.core.exceptions import ObjectDoesNotExist
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.conf import settings
from django.utils.translation import ugettext as _

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.formtools.wizard import FormWizard

from django.middleware.csrf import _get_new_csrf_key

from .forms import AnswerFormSet, UsernameForm, QuestionForm
from .conf import SQ_SESSION_KEY


@login_required
def setup_form(request):
    if request.method == 'POST':
        formset = AnswerFormSet(request.POST, user=request.user)
        if formset.is_valid():
            formset.save_all()
            messages.info(request,
                          _("Your secret answers were successfully saved."))
            return redirect(settings.LOGIN_REDIRECT_URL)
    else:
        formset = AnswerFormSet(user=request.user)
    return render_to_response("secretquestions/setup_form.html", {
        "formset": formset,
        },
        context_instance=RequestContext(request))


class SecretQuestionWizard(FormWizard):
    __name__ = 'SecretQuestionWizard'  # fix for debugtoolbar introspection

    def __init__(self, request):
        self.user = None
        self.redirect = request.get_full_path()

        self.step_mapping = {}

        if request.user.is_authenticated():
            self.user = request.user
            form_list = []
        else:
            if request.POST:
                username = request.POST.get('0-username')
                try:
                    self.user = User.objects.get(username=username)
                except ObjectDoesNotExist:
                    pass
            form_list = [UsernameForm, ]

        if self.user:
            for answer in self.user.secret_answers.all():
                self.step_mapping[len(form_list)] = answer
                form_list.append(QuestionForm)

        super(SecretQuestionWizard, self).__init__(form_list)

    def get_form(self, step, data=None):
        answer = self.step_mapping.get(step, None)
        form = super(SecretQuestionWizard, self).get_form(step, data)
        form.user = self.user
        form.answer = answer
        return form

    def get_template(self, step):
                return 'secretquestions/step.html'

    def done(self, request, form_list):
        # double check validation, it's already done
        # by wizard and redirect on the step with corrupted data
        for form in form_list:
            if not form.is_valid():
                raise Exception('SQ corrupted data')  # pragma: no cover

        token = _get_new_csrf_key()
        path = urlparse(self.redirect).path
        params = parse_qs(urlparse(self.redirect).query,
                          keep_blank_values=True)
        params[SQ_SESSION_KEY] = token
        qs = urlencode(params)
        url = "%s?%s" % (path, qs)
        request.session[SQ_SESSION_KEY] = (token, path, datetime.now(), self.user.pk)

        return redirect(url)

    def __call__(self, request, *args, **kwargs):
        if len(self.form_list) == 0:
            return self.done(request, self.form_list)
        return super(SecretQuestionWizard, self).\
            __call__(request, *args, **kwargs)
